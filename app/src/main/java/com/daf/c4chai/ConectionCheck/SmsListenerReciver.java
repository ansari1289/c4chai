package com.daf.c4chai.ConectionCheck;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.daf.c4chai.interfaces.ReadSmsInterface;

/**
 * Created by faraz on 10/27/2018.
 */

public class SmsListenerReciver extends BroadcastReceiver {

    private static ReadSmsInterface.OTPListener mListener; // this listener will do the magic of throwing the extracted OTP to all the bound views.

    @Override
    public void onReceive(Context context, Intent intent) {

        // this function is trigged when each time a new SMS is received on device.

        Bundle data = intent.getExtras();
        SmsMessage[] smsm = null;
        String sms_str ="";
        Object[] pdus = new Object[0];
        if (data != null) {
            pdus = (Object[]) data.get("pdus"); // the pdus key will contain the newly received SMS
        }

        if (pdus != null) {

            smsm = new SmsMessage[pdus.length];
            for (int i=0; i<smsm.length; i++){
                smsm[i] = SmsMessage.createFromPdu((byte[])pdus[i]);


                sms_str = smsm[i].getMessageBody().toString();

                String Sender = smsm[i].getOriginatingAddress();
                String code=sms_str.substring(sms_str.length() - 6);
                //Check here sender is yours
                Log.v("smsMessage",""+code);
               if(Sender.equalsIgnoreCase("IM-CFCHAI")){
                  if (mListener!=null)
                   mListener.onOTPReceived(code);
               }


            }
            /*for (Object pdu : pdus) { // loop through and pick up the SMS of interest
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdu);
                Log.v("smsMessage",""+smsMessage);
                // your custom logic to filter and extract the OTP from relevant SMS - with regex or any other way.

                if (mListener!=null)
                    mListener.onOTPReceived("Extracted OTP");
                break;
            }*/
        }
    }

    public static void bindListener(ReadSmsInterface.OTPListener listener) {
        mListener = listener;
    }

    public static void unbindListener() {
        mListener = null;
    }
}


