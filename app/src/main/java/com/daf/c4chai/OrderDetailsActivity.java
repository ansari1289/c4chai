package com.daf.c4chai;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daf.c4chai.adapter.OrderDetailAdapter;
import com.daf.c4chai.bean.OrderDetailBean;
import com.daf.c4chai.rest.ApiClient;
import com.daf.c4chai.rest.ApiInterface;
import com.daf.c4chai.utils.AppUtil;
import com.daf.c4chai.utils.Constant;
import com.daf.c4chai.utils.CustomProgressDialog;
import com.daf.c4chai.utils.SessionManager;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OrderDetailsActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView_list)
    RecyclerView recyclerView;

    @BindView(R.id.name_text)
    TextView nameTv;

    @BindView(R.id.date_text)
    TextView dateTv;

    @BindView(R.id.totaltea_text)
    TextView teaCountTV;

    @BindView(R.id.totalcoffee_text)
    TextView coffeeCountTV;

    @BindView(R.id.rootlayout)
    RelativeLayout rootlayout;

    @BindView(R.id.main_layout)
    RelativeLayout mainLayout;

    SessionManager sessionManager;
    CustomProgressDialog mCustomProgressDialog;
    private ArrayList<OrderDetailBean> orderDetailBeans = new ArrayList<>();

    OrderDetailAdapter mAdapter;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        //    sessionManager=new SessionManager(PayActivity.this);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        mCustomProgressDialog = new CustomProgressDialog(this);


        nameTv.setText(getIntent().getExtras().getString("shopkeeperName"));

        dateTv.setText(AppUtil.getDateInFormat("yyyy-MM-dd", "dd/MM/yyyy", getIntent().getExtras().getString("date")));

        if (AppUtil.isInternetAvailable(this)) {
            getOrderDetail(getIntent().getExtras().getString("shopkeeperId"), getIntent().getExtras().getString("date"));
        } else {
            Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
        }
    }


    @OnClick(R.id.backBtn)
    public void onBackClick(View view) {
        onBackPressed();
    }


    private void getOrderDetail(String shopkeeperId, String date) {
        mCustomProgressDialog.showProgressDialog("Loading");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.orderDateList(sessionManager.getUserToken(), shopkeeperId, date);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response1) {

                try {
                    if (response1.isSuccessful()) {
                        mCustomProgressDialog.hideProgressDialog();
                        try {
                            mainLayout.setVisibility(View.VISIBLE);
                            String json = response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            if (response.getString("status").equalsIgnoreCase("200")) {
                                JSONObject dataObject = response.getJSONObject("data");
                                coffeeCountTV.setText("Total Coffee: "+dataObject.optString("total_coffee"));
                                teaCountTV.setText("Total Tea: "+dataObject.optString("total_tea"));
// get category JSONObject from mainJSONObj
                                JSONObject categoryJSONObj=dataObject.getJSONObject("tea_coffee_details");

// get all keys from categoryJSONObj

                                Iterator<String> iterator = categoryJSONObj.keys();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                   // JSONObject mainJSONObj=new JSONObject(key);
                                    JSONObject dateObject=categoryJSONObj.getJSONObject(key);
                                    Log.i("TAG","key:"+key +"--Value::"+categoryJSONObj.optString(key));
                                    orderDetailBeans.add(new OrderDetailBean(key,
                                            dateObject.optString("tea_qty"), dateObject.optString("coffee_qty")));
                                }
                               /* for (int i = 0; i < teaArray.length(); i++) {
                                    JSONObject teaObject = teaArray.getJSONObject(i);
                                    orderDetailBeans.add(new OrderDetailBean(teaObject.optString("timeslot"),
                                            teaObject.optString("tea_qty"), teaObject.optString("coffee_qty")));

                                }*/
                                /*JSONArray coffeeArray = dataObject.getJSONArray("coffee_details");
                                for (int i = 0; i < coffeeArray.length(); i++) {
                                    JSONObject coffeeObject = coffeeArray.getJSONObject(i);
                                    orderDetailBeans.add(new OrderDetailBean(coffeeObject.optString("timeslot"),
                                            dataObject.optString("qty"), dataObject.optString("qty")));
                                }
*/

                                mAdapter = new OrderDetailAdapter(OrderDetailsActivity.this, orderDetailBeans);
                                LinearLayoutManager mLayoutManager = new LinearLayoutManager(OrderDetailsActivity.this, LinearLayoutManager.VERTICAL, false);
                                recyclerView.setLayoutManager(mLayoutManager);
                                recyclerView.setAdapter(mAdapter);
                                //  AppUtil.showToast(getActivity(), response.getString("msg"), true);

                            } else {
                                Constant.showSnackBar(rootlayout, response.getString("msg"));
                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        mCustomProgressDialog.hideProgressDialog();
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                    }


                } catch (Exception e) {
                    mCustomProgressDialog.hideProgressDialog();
                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mCustomProgressDialog.hideProgressDialog();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));


            }
        });

    }

}



