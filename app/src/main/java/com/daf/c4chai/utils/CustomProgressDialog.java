package com.daf.c4chai.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.daf.c4chai.R;
import com.wang.avi.AVLoadingIndicatorView;

/**
 * Created Rupesh Saxena
 */

public class CustomProgressDialog extends Dialog {
    private static Dialog pDialog;
    private Context context;


    public CustomProgressDialog(Context context) {
        super(context);
        this.context = context;
    }

    public CustomProgressDialog(Context context, int theme) {
        super(context, theme);
    }


    public void hideProgressDialog() {
        try {
            if (pDialog != null) {
                if (pDialog.isShowing())
                    pDialog.dismiss();
                pDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showProgressDialog(String message) {
        try {
            if (pDialog == null) {
                pDialog = createProgressDialog(context, false, message);
                pDialog.setCanceledOnTouchOutside(false);
                pDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private CustomProgressDialog createProgressDialog(Context context, boolean cancelable, String message) {

        CustomProgressDialog dialog = new CustomProgressDialog(context, R.style.CustomProgressDialog);
        dialog.setTitle("");
        dialog.setContentView(R.layout.dialog_custome_progress);
        dialog.setCancelable(cancelable);
        dialog.setCanceledOnTouchOutside(cancelable);
        TextView textOfLoader = dialog.findViewById(R.id.textOfLoader);
        AVLoadingIndicatorView avi= (AVLoadingIndicatorView) findViewById(R.id.avi);
        textOfLoader.setText(message);
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount = 0.4f;
        dialog.getWindow().setAttributes(lp);

       /* Animation a = AnimationUtils.loadAnimation(getContext(), R.anim.progress_anim);
        a.setDuration(1000);
        loader.startAnimation(a);*/

        return dialog;
    }

    public void setMessage(CharSequence message) {
        if (message != null && message.length() > 0) {
            TextView txt = findViewById(R.id.textOfLoader);
            txt.setText(message);
            txt.invalidate();
        }
    }
}

