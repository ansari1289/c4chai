package com.daf.c4chai.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.daf.c4chai.SplashActivity;


public class SessionManager {

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "C4ChaiAppPref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "isLoggedIn";
    private static final String KEY_USER_ID = "pateintID";
    private static final String KEY_TOKEN = "token";
    private static final String KEY_MOBILE="mobilenumber";
    private static final String KEY_FUll_NAME = "name";
    private static final String KEY_FIRETOKEN = "firtoken";
    private static final String KEY_COFFEE_RATE="coffee_rate";
    private static final String KEY_TEA_RATE="tea_rate";

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    public void createSession(String userid, String usertoken, String username, String usermobile, String rate_tea, String rate_coffee) {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USER_ID,userid);
        editor.putString(KEY_FUll_NAME,username);
        editor.putString(KEY_TOKEN,usertoken);
        editor.putString(KEY_MOBILE,usermobile);
        editor.putString(KEY_COFFEE_RATE,rate_coffee);
        editor.putString(KEY_TEA_RATE,rate_tea);
        editor.apply();
    }

    public void setMobileNumber(String mobileNumber){
        editor.putString(KEY_MOBILE, mobileNumber);
        editor.apply();
    }

    public String getMobileNumber(){
        return pref.getString(KEY_MOBILE,"");
    }

    public void setTeaRate(String teaRate){
        editor.putString(KEY_TEA_RATE, teaRate);
        editor.apply();
    }

    public String getTeaRate(){
        return pref.getString(KEY_TEA_RATE,"");
    }

    public void setCoffeeRate(String coffeeRate){
        editor.putString(KEY_COFFEE_RATE, coffeeRate);
        editor.apply();
    }

    public String getCoffeeRate(){
        return pref.getString(KEY_COFFEE_RATE,"");
    }

    public String getUserName(){
        return pref.getString(KEY_FUll_NAME,"");
    }

    public void setUserName(String coffeeRate){
        editor.putString(KEY_FUll_NAME, coffeeRate);
        editor.apply();
    }


    public void setUserToken(String token){
        editor.putString(KEY_TOKEN, token);
        editor.apply();
    }

    public String getUserToken(){
        return pref.getString(KEY_TOKEN,"");
    }

    public void setUserId(String userid){
        editor.putString(KEY_USER_ID, userid);
        editor.apply();
    }

    public String getUseId(){
        return pref.getString(KEY_USER_ID,"");
    }

    public boolean isLogin() {
        return pref.getBoolean(IS_LOGIN, false);
    }


    public void setLogin(boolean token) {
        editor.putBoolean(IS_LOGIN, token);
        editor.apply();
    }


    public void setFirebaseToken(String firetoken){
        editor.putString(KEY_FIRETOKEN, firetoken);
        editor.apply();
    }

    public String getFirebaseToken(){
       return pref.getString(KEY_FIRETOKEN,"");
    }


    /**
     * Clear session details
     */
    public void logoutUser(Activity mcontex) {
        editor.clear();
        editor.apply();
        Intent intent = new Intent(mcontex, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        mcontex.finish();
        mcontex.startActivity(intent);
    }
}