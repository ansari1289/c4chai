package com.daf.c4chai.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;

public class Res extends Resources {

    Context context;
    String colorCode;
    public Res(Resources original, Context context, String color) {
        super(original.getAssets(), original.getDisplayMetrics(), original.getConfiguration());
        this.context = context;
        this.colorCode = color;
    }

    @Override
    public int getColor(int id) throws NotFoundException {
        return getColor(id, null);
    }

    @Override
    public int getColor(int id, Theme theme) throws NotFoundException {
        switch (getResourceEntryName(id)) {
            case "colorPrimary":
                // You can change the return value to an instance field that loads from SharedPreferences.
                int cc = Color.parseColor(colorCode);
                return cc; // used as an example. Change as needed.
            default:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    return super.getColor(id, theme);
                }
                return super.getColor(id);
        }
    }
}