package com.daf.c4chai;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.daf.c4chai.rest.ApiClient;
import com.daf.c4chai.rest.ApiInterface;
import com.daf.c4chai.utils.AppLog;
import com.daf.c4chai.utils.AppUtil;
import com.daf.c4chai.utils.Constant;
import com.daf.c4chai.utils.SessionManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by faraz on 10/21/2018.
 */

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.edt_phonenumber)
    EditText edt_phonenumber;

    @BindView(R.id.rootlayout)
    RelativeLayout rootlayout;

    SessionManager sessionManager;
    ProgressDialog progressDialog;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sessionManager=new SessionManager(LoginActivity.this);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_login)
    public void onButtonClick(View view) {
        Constant.hideKeyBoard(LoginActivity.this);
        if (edt_phonenumber.getText().toString().trim().equalsIgnoreCase("")) {
            Constant.showSnackBar(rootlayout, getResources().getString(R.string.please_enter_otp));
        } else if (edt_phonenumber.getText().toString().trim().length() < 10) {
            Constant.showSnackBar(rootlayout, getResources().getString(R.string.please_enter_valid_otp));
        } else {
            if(AppUtil.isInternetAvailable(LoginActivity.this)){
                sendOtpApi(edt_phonenumber.getText().toString().trim());
            }else{
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
            }



        }

    }

    private void sendOtpApi(String mobilenumber) {
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setTitle(getResources().getString(R.string.pleasewait));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.sendOtp(Constant.TOKEN_WITHOUTLOGIN, mobilenumber,AppUtil.getDeviceID(LoginActivity.this), FirebaseInstanceId.getInstance().getToken());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response1) {

                try {
                    if (response1.isSuccessful()) {
                        progressDialog.dismiss();
                        try {
                            String json = response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            JSONObject myobj=response.getJSONObject("data");
                            if (response.getString("status").equalsIgnoreCase("200")) {
                              //  AppUtil.showToast(LoginActivity.this, response.getString("msg"), true);
                                Intent intent = new Intent(LoginActivity.this, VerifyOtpActivity.class);
                                intent.putExtra("mobilenumber",myobj.getString("mobile"));
                                startActivity(intent);
                                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                                finish();
                            } else {
                                Constant.showSnackBar(rootlayout, response.getString("msg"));
                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        progressDialog.dismiss();
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                        AppLog.v("alltool", " not found");
                    }


                } catch (Exception e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));


            }
        });
    }
}

