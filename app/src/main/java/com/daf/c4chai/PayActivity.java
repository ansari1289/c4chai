package com.daf.c4chai;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daf.c4chai.bean.OrderBean;
import com.daf.c4chai.rest.ApiClient;
import com.daf.c4chai.rest.ApiInterface;
import com.daf.c4chai.utils.AppUtil;
import com.daf.c4chai.utils.Constant;
import com.daf.c4chai.utils.CustomProgressDialog;
import com.daf.c4chai.utils.SessionManager;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PayActivity extends AppCompatActivity {

    @BindView(R.id.name_text)
    TextView tvName;

    @BindView(R.id.startDate)
    TextView startDate;

    @BindView(R.id.endDate)
    TextView endDate;

    @BindView(R.id.tea_amount)
    TextView teaAmount;

    @BindView(R.id.coffee_amount)
    TextView coffeeAmount;

    @BindView(R.id.previous_amount)
    TextView previousAmount;

    @BindView(R.id.current_amount)
    TextView currentAmount;

    @BindView(R.id.total_amount)
    TextView totalAmount;

    @BindView(R.id.editAmonut)
    EditText editAmount;

    @BindView(R.id.rootlayout)
    RelativeLayout rootlayout;

    @BindView(R.id.main_layout)
    RelativeLayout mainLayout;


    SessionManager sessionManager;
    CustomProgressDialog mCustomProgressDialog;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);
        sessionManager = new SessionManager(this);
        mCustomProgressDialog = new CustomProgressDialog(this);
        ButterKnife.bind(this);
        tvName.setText(getIntent().getExtras().getString("shopkeeperName"));

        if (AppUtil.isInternetAvailable(this)) {
            getPaymentDetails(getIntent().getExtras().getString("shopkeeperId"));
        } else {
            Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
        }
    }

    @OnClick(R.id.backBtn)
    public void onBackClick(View view) {
        onBackPressed();
    }

    @OnClick(R.id.payBtn)
    public void onPayClick(View view) {
        if (editAmount.getText().toString().trim().equalsIgnoreCase("") || editAmount.getText().toString().trim().startsWith("0")) {
            Constant.showSnackBar(rootlayout, getResources().getString(R.string.please_enter_amount));
        }else{
            if (Float.parseFloat(totalAmount.getText().toString())!=0.0) {
            if (Float.parseFloat(totalAmount.getText().toString())>= Float.parseFloat(editAmount.getText().toString())) {
                if (AppUtil.isInternetAvailable(this)) {
                    addPayment(getIntent().getExtras().getString("shopkeeperId"), editAmount.getText().toString().trim());
                } else {
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
                }
            }else {
                Constant.showSnackBar(rootlayout, "Pay amount should not be greater than total amount");
            }
            }else {
                Constant.showSnackBar(rootlayout, "No outstanding amount remaining");
            }
        }
    }

    private void getPaymentDetails(String shopkeeperId) {
        mCustomProgressDialog.showProgressDialog("Loading");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.getPaymentDetails(sessionManager.getUserToken(), shopkeeperId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response1) {

                try {
                    if (response1.isSuccessful()) {
                        mCustomProgressDialog.hideProgressDialog();
                        try {
                            mainLayout.setVisibility(View.VISIBLE);
                            String json = response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            if (response.getString("status").equalsIgnoreCase("200")) {
                                JSONObject jsonObject = response.getJSONObject("data");
                                teaAmount.setText(jsonObject.optString("total_tea"));
                                coffeeAmount.setText(jsonObject.optString("total_coffee"));
                                Double totalAmountd = Double.parseDouble(jsonObject.optString("total_amount")) ;
                                currentAmount.setText(""+ totalAmountd.doubleValue());
                                Double totalOutstanding = Double.parseDouble(jsonObject.optString("total_outstanding")) ;
                                previousAmount.setText(""+ totalOutstanding.doubleValue());
                                int toatl = Integer.parseInt(jsonObject.optString("total_amount"))+ Integer.parseInt(jsonObject.optString("total_outstanding"));
                                Double added = Double.valueOf(toatl);
                                totalAmount.setText("" +added.doubleValue());
                                Calendar calendar = Calendar.getInstance();
                                Date c = Calendar.getInstance().getTime();
                                String formattedDate ="" ;
                                String output = "";
                                SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
                                if (!jsonObject.optString("last_payment_date").equalsIgnoreCase("null")) {
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                    try {
                                        calendar.setTime(sdf.parse(jsonObject.optString("last_payment_date")));

                                      //  Date serverDate = calendar.getTime();
                                        formattedDate = sdf1.format(c);
                                        output = sdf1.format(calendar.getTime());

                                        Date date1 = sdf1.parse(output);

                                        // String str2 = "13/10/2013";
                                        Date date2 = sdf1.parse(formattedDate);
                                        if (date2.before(date1)) {
                                            calendar.add(Calendar.DATE, 1);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }else {
                                    formattedDate = sdf1.format(c);
                                    output = sdf1.format(calendar.getTime());
                                }


                                startDate.setText(output);

                               // String str1 = "12/10/2013";



                                    System.out.println("Current time => " + c);

                                 //   SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

                                    endDate.setText(formattedDate);

                            } else {
                                Constant.showSnackBar(rootlayout, response.getString("msg"));
                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        mCustomProgressDialog.hideProgressDialog();
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                    }


                } catch (Exception e) {
                    mCustomProgressDialog.hideProgressDialog();
                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mCustomProgressDialog.hideProgressDialog();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));

            }
        });

    }

    private void addPayment(String shopkeeperId, String amount) {
        mCustomProgressDialog.showProgressDialog("Loading");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.addPayment(sessionManager.getUserToken(), shopkeeperId, amount);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response1) {

                try {
                    if (response1.isSuccessful()) {
                        mCustomProgressDialog.hideProgressDialog();
                        try {
                            String json = response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            if (response.getString("status").equalsIgnoreCase("200")) {
                                AppUtil.showToast(PayActivity.this, response.getString("msg"), true);
                                     finish();
                            } else {
                                Constant.showSnackBar(rootlayout, response.getString("msg"));
                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        mCustomProgressDialog.hideProgressDialog();
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                    }


                } catch (Exception e) {
                    mCustomProgressDialog.hideProgressDialog();
                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mCustomProgressDialog.hideProgressDialog();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));


            }
        });

    }


}

