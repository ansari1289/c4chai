package com.daf.c4chai.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daf.c4chai.R;
import com.daf.c4chai.bean.PaymentHistoryBean;
import com.daf.c4chai.utils.AppUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.ViewHolder> {

    private Context context;
    private ArrayList<PaymentHistoryBean> paymentHistoryBeans;

    public PaymentAdapter(Context context, ArrayList<PaymentHistoryBean> paymentHistoryBeans) {
        this.context = context;
        this.paymentHistoryBeans = paymentHistoryBeans;
    }

    @Override
    public PaymentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_payment_list, parent, false);
        return new PaymentAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PaymentAdapter.ViewHolder holder, final int position) {
        holder.tv_totalAmount.setText(paymentHistoryBeans.get(position).getAmount());
        holder.date.setText(AppUtil.getDateInFormat("yyyy-MM-dd","dd/MM/yyyy",paymentHistoryBeans.get(position).getPayment_date()));
    }


    @Override
    public int getItemCount() {
        return paymentHistoryBeans.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        @BindView(R.id.dateText)
        TextView date;
        @BindView(R.id.total_amount)
        TextView tv_totalAmount;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);
        }

    }
}
