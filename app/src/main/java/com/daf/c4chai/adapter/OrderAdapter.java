package com.daf.c4chai.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daf.c4chai.OrderDetailsActivity;
import com.daf.c4chai.R;
import com.daf.c4chai.bean.OrderBean;
import com.daf.c4chai.utils.AppUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    Context context;
    private ArrayList<OrderBean> orderBeans;
    private String shopkeeperName;

    public OrderAdapter(Context context, ArrayList<OrderBean> orderBeans, String shopkeeperName) {
        this.context = context;
        this.orderBeans = orderBeans;
        this.shopkeeperName = shopkeeperName;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rows_order_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tv_amount.setText(orderBeans.get(position).getTotal_amount());
        holder.tv_date.setText(AppUtil.getDateInFormat("yyyy-MM-dd","dd/MM/yyyy",orderBeans.get(position).getOrder_date()));
        holder.tv_quantity.setText(orderBeans.get(position).getOrder_quantity());

        holder.rl_root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,OrderDetailsActivity.class);
                intent.putExtra("shopkeeperId",orderBeans.get(position).getShop_id());
                intent.putExtra("shopkeeperName",shopkeeperName);
                intent.putExtra("date",orderBeans.get(position).getOrder_date());
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return orderBeans.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        @BindView(R.id.dateText)
        TextView tv_date;
        @BindView(R.id.quantityText)
        TextView tv_quantity;
        @BindView(R.id.total_amount)
        TextView tv_amount;

        @BindView(R.id.rootView)
        RelativeLayout rl_root;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);
        }

    }
}
