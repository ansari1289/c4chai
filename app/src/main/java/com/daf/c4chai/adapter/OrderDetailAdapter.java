package com.daf.c4chai.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daf.c4chai.R;
import com.daf.c4chai.bean.OrderBean;
import com.daf.c4chai.bean.OrderDetailBean;
import com.daf.c4chai.utils.AppUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.ViewHolder> {

    Context context;
    private ArrayList<OrderDetailBean> orderBeans;

    public OrderDetailAdapter(Context context, ArrayList<OrderDetailBean> orderBeans) {
        this.context = context;
        this.orderBeans = orderBeans;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_datewise_order, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tv_time.setText(AppUtil.getDateInFormat("yyyy-MM-dd HH:mm:ss", "hh:mm a",orderBeans.get(position).getTimeslot()));
        if (Integer.parseInt(orderBeans.get(position).getTea_qty()) == 0)
        {
            holder.rlTea.setBackgroundColor(context.getResources().getColor(R.color.colorGray));
            holder.tv_teaquantityText.setText("- -");

        }
        else {
            holder.tv_teaquantityText.setText(orderBeans.get(position).getTea_qty());
            holder.rlTea.setBackgroundColor(context.getResources().getColor(R.color.orange));
        }

        if (Integer.parseInt(orderBeans.get(position).getCoffee_qty()) == 0)
        {
            holder.rlCoffee.setBackgroundColor(context.getResources().getColor(R.color.colorGray));
            holder.tv_coffeequantityText.setTextColor(context.getResources().getColor(R.color.black));
            holder.tv_coffeequantityText.setText("- -");

        }
        else {
            holder.tv_coffeequantityText.setText(orderBeans.get(position).getCoffee_qty());
            holder.rlCoffee.setBackgroundColor(context.getResources().getColor(R.color.choclate));
            holder.tv_coffeequantityText.setTextColor(context.getResources().getColor(R.color.white));
        }
    }


    @Override
    public int getItemCount() {
        return orderBeans.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        @BindView(R.id.timeText)
        TextView tv_time;
        @BindView(R.id.teaquantityText)
        TextView tv_teaquantityText;
        @BindView(R.id.coffeequantityText)
        TextView tv_coffeequantityText;

        @BindView(R.id.rl_tea)
        RelativeLayout rlTea;

        @BindView(R.id.rl_coffee)
        RelativeLayout rlCoffee;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);
        }

    }
}

