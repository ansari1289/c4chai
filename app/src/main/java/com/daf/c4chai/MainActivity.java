package com.daf.c4chai;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daf.c4chai.fragments.AddShopKeeperFragment;
import com.daf.c4chai.fragments.HomeFragment;
import com.daf.c4chai.fragments.ListFragment;
import com.daf.c4chai.fragments.ProfileFragment;
import com.daf.c4chai.rest.ApiClient;
import com.daf.c4chai.rest.ApiInterface;
import com.daf.c4chai.utils.AppLog;
import com.daf.c4chai.utils.AppUtil;
import com.daf.c4chai.utils.Constant;
import com.daf.c4chai.utils.CustomProgressDialog;
import com.daf.c4chai.utils.CustomTypefaceSpan;
import com.daf.c4chai.utils.SessionManager;
import com.google.gson.JsonObject;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.bnve)
    BottomNavigationViewEx bnve;

    @BindView(R.id.iv_search)
    ImageView iv_search;

    @BindView(R.id.rootlayout)
    RelativeLayout rootlayout;

    SessionManager sessionManager;
    CustomProgressDialog mCustomProgressDialog;

    @BindView(R.id.fab)
    FloatingActionButton fab;
    Menu menu;

    final String regExp = "[0-9]+([,.][0-9]{1,2})?";
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        //  bnve.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //  loadFragment(new HomeFragment());
        sessionManager=new SessionManager(this);
        mCustomProgressDialog = new CustomProgressDialog(this);
        initView();
    }

    @OnClick(R.id.logout)
    public void onBackClick(View view) {
        sessionManager.logoutUser(MainActivity.this);
    }


    private void initView() {

        bnve.enableItemShiftingMode(false);
        bnve.enableShiftingMode(false);
        bnve.enableAnimation(false);

       setupNavigationView();
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold.ttf");
        CustomTypefaceSpan typefaceSpan = new CustomTypefaceSpan("", font);
        for (int i = 0; i <bnve.getMenu().size(); i++) {
            MenuItem menuItem = bnve.getMenu().getItem(i);
            SpannableStringBuilder spannableTitle = new SpannableStringBuilder(menuItem.getTitle());
            spannableTitle.setSpan(typefaceSpan, 0, spannableTitle.length(), 0);
            menuItem.setTitle(spannableTitle);
        }
    }

    private void setupNavigationView() {
        if (bnve != null) {
            // centerMenuIcon();
            /*BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) bnve.getChildAt(0);
            View notification_v = bottomNavigationMenuView.getChildAt(2);
            View mytool_v = bottomNavigationMenuView.getChildAt(1);
            View message_v = bottomNavigationMenuView.getChildAt(3);
            View myrental_v = bottomNavigationMenuView.getChildAt(4);

            BottomNavigationItemView notification_itemView = (BottomNavigationItemView) notification_v;
            BottomNavigationItemView mytool_itemView = (BottomNavigationItemView) mytool_v;
            BottomNavigationItemView message_itemView = (BottomNavigationItemView) message_v;
            BottomNavigationItemView myretal_itemView = (BottomNavigationItemView) myrental_v;

            View notification_badge = LayoutInflater.from(this).inflate(R.layout.badge_layout, bottomNavigationMenuView, false);
            View mytool_badge = LayoutInflater.from(this).inflate(R.layout.badge_layout, bottomNavigationMenuView, false);
            View message_badge = LayoutInflater.from(this).inflate(R.layout.badge_layout, bottomNavigationMenuView, false);
            View myrental_badge = LayoutInflater.from(this).inflate(R.layout.badge_layout, bottomNavigationMenuView, false);

            notification_itemView.addView(notification_badge);
            mytool_itemView.addView(mytool_badge);
            message_itemView.addView(message_badge);
            myretal_itemView.addView(myrental_badge);

            notification_badgecount = (TextView) notification_badge.findViewById(R.id.notificationsbadge);
            mytool_badgecount = (TextView) mytool_badge.findViewById(R.id.notificationsbadge);
            message_badgecount = (TextView) message_badge.findViewById(R.id.notificationsbadge);
            myrental_badgecount = (TextView) myrental_badge.findViewById(R.id.notificationsbadge);
            changebage(notification_count, message_count, mytool_count, myrental_count);*/
            // Select first menu item by default and show Fragment accordingly.
            menu = bnve.getMenu();
            selectFragment(menu.getItem(0));
            int s = bnve.getSelectedItemId();
            // Set action to perform when any menu-item is selected.
            bnve.setOnNavigationItemSelectedListener(
                    new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                            selectFragment(item);
                            return false;
                        }
                    });
        }
    }


    protected void selectFragment(MenuItem item) {
        Class fragmentClass = null;


        switch (item.getItemId()) {
            case R.id.i_add:
                item.setChecked(true);
                // Action to perform when Home Menu item is selected.
                fragmentClass = AddShopKeeperFragment.class;
                insertFragment("Addshopkeeper", fragmentClass);
                HideSerchbutton();
                break;
            case R.id.i_profile:
                // Action to perform when Bag Menu item is selected.
                //   pushFragment(new BagFragment());
                item.setChecked(true);
                fragmentClass = ProfileFragment.class;
                insertFragment("profile", fragmentClass);
                HideSerchbutton();

                break;
            case R.id.i_list:
                item.setChecked(true);
                fragmentClass = ListFragment.class;
                insertFragment("List", fragmentClass);
                ShowSerchbutton();
                break;
            case R.id.i_home:
                ShowSerchbutton();
                item.setChecked(true);
                fragmentClass = HomeFragment.class;
                insertFragment("Home", fragmentClass);

                break;


        }
    }

    private void insertFragment(String tag, Class fragmentClass) {

        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment_add_tool_main by replacing any existing fragment_add_tool_main
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            // ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
            ft.replace(R.id.frame_container, fragment, tag)
                    .addToBackStack(fragmentClass.getName())
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.i_add:

                    //Fragment currentFragment = getActivity().getFragmentManager().findFragmentById(R.id.fragment_container);
                    *//*if (currentFragment instanceof NameOfYourFragmentClass) {
                        Log.v(TAG, "find the current fragment");
                    }*//*
                    fragment = new AddShopKeeperFragment();
                    loadFragment(fragment);
                    HideSerchbutton();
                    return true;
                case R.id.i_profile:

                    fragment = new ProfileFragment();
                    loadFragment(fragment);
                    HideSerchbutton();
                    return true;
                case R.id.i_list:

                    fragment = new ListFragment();
                    loadFragment(fragment);
                    ShowSerchbutton();
                    return true;

                case R.id.i_home:

                    fragment = new HomeFragment();
                    loadFragment(fragment);
                    ShowSerchbutton();
                    return true;
            }

            return false;
        }
    };
    private void loadFragment(Fragment fragment) {

        //Fragment currentFragment = getActivity().getFragmentManager().findFragmentById(R.id.fragment_container);
                    *//*if (currentFragment instanceof NameOfYourFragmentClass) {
                        Log.v(TAG, "find the current fragment");
                    }*//*
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }*/


    @OnClick({R.id.fab, R.id.iv_search})
    public void onButtonClick(View view) {
        switch (view.getId()) {
            case R.id.fab:

                break;
            case R.id.iv_search:
                if (getVisibleFragment() instanceof ListFragment) {
                    ((ListFragment) getVisibleFragment()).changeButtonVisibility(true);
                    iv_search.setVisibility(View.GONE);
                    // Logic here...
                } else if (getVisibleFragment() instanceof HomeFragment) {
                    ((HomeFragment) getVisibleFragment()).changeButtonVisibility(true);
                    iv_search.setVisibility(View.GONE);
                    // Logic here...
                }
                break;

        }

    }


    public void ShowSerchbutton() {
        iv_search.setVisibility(View.VISIBLE);
    }

    public void HideSerchbutton() {
        iv_search.setVisibility(View.GONE);
    }

    private Fragment getVisibleFragment() {
        FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        for (Fragment fragment : fragments) {
            if (fragment != null && fragment.isVisible())
                return fragment;
        }
        return null;
    }


    @Override
    public void onBackPressed() {

        menu = bnve.getMenu();
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        if (f instanceof HomeFragment) {
            finish();

        } else if (f instanceof ListFragment) {
            selectFragment(menu.getItem(0));
        } else if (f instanceof AddShopKeeperFragment) {
            selectFragment(menu.getItem(0));
        } else if (f instanceof ProfileFragment) {
            selectFragment(menu.getItem(0));
        }

    }

   /* private void addGlobalRateDialog() {

        final Dialog d = new Dialog(this);
        d.setContentView(R.layout.dialog_add_coffee_tea_amount);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        d.setCancelable(false);
       d.getWindow().getAttributes().windowAnimations = R.style.Animations_SmileWindow;
        final EditText teaEditAmount = d.findViewById(R.id.editTeaAmount);
        final EditText coffeeEditAmount = d.findViewById(R.id.editCoffeeAmount);
        TextView buttonAdd = d.findViewById(R.id.btndone);
        final Pattern pattern = Pattern.compile(regExp);

// This can be repeated in a loop with different inputs:
        final Matcher teaMatcher = pattern.matcher(teaEditAmount.getText().toString());
        final Matcher coffeeMatcher = pattern.matcher(coffeeEditAmount.getText().toString());
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (teaEditAmount.getText().toString().trim().equalsIgnoreCase("") || teaEditAmount.getText().toString().trim().startsWith("0")) {
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.please_enter_tea_amount), Toast.LENGTH_SHORT).show();
                }  else if (coffeeEditAmount.getText().toString().trim().equalsIgnoreCase("") || coffeeEditAmount.getText().toString().trim().startsWith("0")) {
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.please_enter_coffee_amount), Toast.LENGTH_SHORT).show();
                }  else {
                    d.dismiss();
                    if (AppUtil.isInternetAvailable(MainActivity.this)) {
                        addTeaCoffeeRateApi(teaEditAmount.getText().toString().trim(), coffeeEditAmount.getText().toString());
                    } else {
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
                    }

                }
            }
        });
        d.show();
    }

    private void addTeaCoffeeRateApi(final String teaAmount, final String coffeeAmount) {

        mCustomProgressDialog.showProgressDialog("Loading");
        String token = sessionManager.getUserToken();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.addItemsRate(token, teaAmount, coffeeAmount);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response1) {

                try {
                    if (response1.isSuccessful()) {
                        mCustomProgressDialog.hideProgressDialog();
                        try {
                            String json = response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            JSONObject myobj = response.getJSONObject("data");
                            if (response.getString("status").equalsIgnoreCase("200")) {
                                AppUtil.showToast(MainActivity.this, response.getString("msg"), true);
                                setupNavigationView();
                                sessionManager.setCoffeeRate(coffeeAmount);
                                sessionManager.setTeaRate(teaAmount);
                            } else {
                                Constant.showSnackBar(rootlayout, response.getString("msg"));
                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        mCustomProgressDialog.hideProgressDialog();
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                        AppLog.v("alltool", " not found");
                    }


                } catch (Exception e) {
                    mCustomProgressDialog.hideProgressDialog();
                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mCustomProgressDialog.hideProgressDialog();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));


            }
        });
    }


    private void globalRateApi() {
        mCustomProgressDialog.showProgressDialog("Loading");
        String token = sessionManager.getUserToken();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.checkGlobalRate(token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response1) {
                try {
                    if (response1.isSuccessful()) {
                        mCustomProgressDialog.hideProgressDialog();
                        try {
                            String json = getResponseBody(response1);//response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            if (response.getString("status").equalsIgnoreCase("200")) {
                                //  AppUtil.showToast(MainActivity.this, response.getString("msg"), true);
                                JSONObject dataObject = response.getJSONObject("data");
                                String tea = dataObject.getString("Tea");
                                String coffee = dataObject.getString("Coffee");
                                if (tea.equalsIgnoreCase("-1")&& coffee.equalsIgnoreCase("-1"))
                                {
                                    addGlobalRateDialog();
                                }else {
                                    sessionManager.setCoffeeRate(coffee);
                                    sessionManager.setTeaRate(tea);
                                    setupNavigationView();
                                }

                            } else {
                                Constant.showSnackBar(rootlayout, response.getString("msg"));
                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        mCustomProgressDialog.hideProgressDialog();
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                        AppLog.v("alltool", " not found");
                    }


                } catch (Exception e) {
                    mCustomProgressDialog.hideProgressDialog();
                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mCustomProgressDialog.hideProgressDialog();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));


            }
        });
    }

    public static String getResponseBody(Response<ResponseBody> response) {

        BufferedReader reader = null;
        String output = null;
        try {

            if (response.body() != null) {
                reader = new BufferedReader(new InputStreamReader(response.body().byteStream()));
            } else if (response.errorBody() != null) {
                reader = new BufferedReader(new InputStreamReader(response.errorBody().byteStream()));
            }
            output = reader.readLine();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return output;
    }*/

}
