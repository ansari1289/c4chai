package com.daf.c4chai;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daf.c4chai.adapter.OrderAdapter;
import com.daf.c4chai.adapter.PaymentAdapter;
import com.daf.c4chai.bean.OrderBean;
import com.daf.c4chai.bean.PaymentHistoryBean;
import com.daf.c4chai.rest.ApiClient;
import com.daf.c4chai.rest.ApiInterface;
import com.daf.c4chai.utils.AppUtil;
import com.daf.c4chai.utils.Constant;
import com.daf.c4chai.utils.CustomProgressDialog;
import com.daf.c4chai.utils.SessionManager;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OrderPaymentActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView_list)
    RecyclerView recyclerView;

   /* @BindView(R.id.iv_teaadd)
    ImageView addIV;
*/
    @BindView(R.id.ordersButton)
    TextView ordersButton;

    @BindView(R.id.paymentButton)
    TextView paymentButton;

    @BindView(R.id.name_text)
    TextView nameTV;

    @BindView(R.id.rootlayout)
    RelativeLayout rootlayout;


    @BindView(R.id.main_layout)
    LinearLayout mainLayout;

    @BindView(R.id.nodataAvailable)
    TextView noDataAvailable;

    @BindView(R.id.number_text)
    TextView number;

    SessionManager sessionManager;
    CustomProgressDialog mCustomProgressDialog;
    private ArrayList<OrderBean> orderBeans = new ArrayList<>();
    private ArrayList<PaymentHistoryBean> paymentHistoryBeans = new ArrayList<>();
    OrderAdapter mAdapter;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_payment);
        //    sessionManager=new SessionManager(PayActivity.this);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        mCustomProgressDialog = new CustomProgressDialog(this);


        nameTV.setText(getIntent().getExtras().getString("shopkeeperName"));
        number.setText(getIntent().getExtras().getString("shopkeeperNumber"));

        if (AppUtil.isInternetAvailable(this)) {
            getOrders(getIntent().getExtras().getString("shopkeeperId"));
        } else {
            Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
        }
    }

    @OnClick(R.id.ordersButton)
    public void onButtonClick(View view) {
        ordersButton.setBackgroundResource(R.drawable.br_black_corners);
        ordersButton.setTextColor(getResources().getColor(R.color.white));
        paymentButton.setBackgroundResource(R.drawable.dr_white_black_border);
        paymentButton.setTextColor(getResources().getColor(R.color.black));


        if (AppUtil.isInternetAvailable(this)) {
            getOrders(getIntent().getExtras().getString("shopkeeperId"));
        } else {
            Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
        }
        /*OrderAdapter mAdapter = new OrderAdapter(this, orderBeans, getIntent().getExtras().getString("shopkeeperName"));
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);*/
    }

    @OnClick(R.id.paymentButton)
    public void onPaymentButtonClick(View view) {

        paymentButton.setBackgroundResource(R.drawable.left_red_dr);
        paymentButton.setTextColor(getResources().getColor(R.color.white));
        ordersButton.setBackgroundResource(R.drawable.left_white_drable);
        ordersButton.setTextColor(getResources().getColor(R.color.black));

        if (AppUtil.isInternetAvailable(this)) {
            getPaymentHistory(getIntent().getExtras().getString("shopkeeperId"));
        } else {
            Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
        }

    }

    @OnClick(R.id.backBtn)
    public void onBackClick(View view) {
        onBackPressed();
    }


    private void getOrders(String shopkeeperId) {
        mCustomProgressDialog.showProgressDialog("Loading");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.orderList(sessionManager.getUserToken(), shopkeeperId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response1) {

                try {
                    if (response1.isSuccessful()) {
                        orderBeans.clear();
                        mCustomProgressDialog.hideProgressDialog();
                        try {
                            mainLayout.setVisibility(View.VISIBLE);
                            String json = response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            if (response.getString("status").equalsIgnoreCase("200")) {
                               /* String data = "{ ... }";
                                Object json = new JSONTokener(data).nextValue();
                                if (json instanceof JSONArray)
                                //you have an object*/

                                JSONArray dataArray = response.optJSONArray("data");
                                if (dataArray!=null && dataArray.length()>0) {
                                    for (int i = 0; i < dataArray.length(); i++) {
                                        JSONObject dataObject = dataArray.getJSONObject(i);
                                        orderBeans.add(new OrderBean(dataObject.optString("user_id"),
                                                dataObject.optString("shop_id"),
                                                dataObject.optString("order_date"),
                                                dataObject.optString("total_amount"),
                                                dataObject.optString("order_quantity")
                                        ));

                                    }
                                    mAdapter = new OrderAdapter(OrderPaymentActivity.this, orderBeans, getIntent().getExtras().getString("shopkeeperName"));
                                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(OrderPaymentActivity.this, LinearLayoutManager.VERTICAL, false);
                                    recyclerView.setLayoutManager(mLayoutManager);
                                    recyclerView.setAdapter(mAdapter);
                                    if (orderBeans.size()>0){
                                        noDataAvailable.setVisibility(View.GONE);
                                    }else {
                                        noDataAvailable.setText("No Orders Available");
                                        noDataAvailable.setVisibility(View.VISIBLE);
                                    }
                                    //  AppUtil.showToast(getActivity(), response.getString("msg"), true);
                                }else {
                                    noDataAvailable.setText("No Orders Available");
                                    noDataAvailable.setVisibility(View.VISIBLE);
                                }

                            } else {
                                noDataAvailable.setText("No Orders Available");
                                noDataAvailable.setVisibility(View.VISIBLE);
                                Constant.showSnackBar(rootlayout, response.getString("msg"));
                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        mCustomProgressDialog.hideProgressDialog();
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                    }


                } catch (Exception e) {
                    mCustomProgressDialog.hideProgressDialog();
                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mCustomProgressDialog.hideProgressDialog();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));


            }
        });

    }

    private void getPaymentHistory(String shopkeeperId) {
        mCustomProgressDialog.showProgressDialog("Loading");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.getPaymentHistory(sessionManager.getUserToken(), shopkeeperId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response1) {

                try {
                    if (response1.isSuccessful()) {
                        mCustomProgressDialog.hideProgressDialog();
                        try {
                            paymentHistoryBeans.clear();
                            String json = response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            if (response.getString("status").equalsIgnoreCase("200")) {
                                JSONObject jsonObject = response.getJSONObject("data");
                                JSONArray dataArray = jsonObject.getJSONArray("payment_history");
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject dataObject = dataArray.getJSONObject(i);
                                    paymentHistoryBeans.add(new PaymentHistoryBean(dataObject.optString("user_id"),
                                            dataObject.optString("shop_id"),
                                            dataObject.optString("amount"),
                                            dataObject.optString("payment_date")));

                                }
                                PaymentAdapter mAdapter = new PaymentAdapter(OrderPaymentActivity.this, paymentHistoryBeans);
                                LinearLayoutManager mLayoutManager = new LinearLayoutManager(OrderPaymentActivity.this, LinearLayoutManager.VERTICAL, false);
                                recyclerView.setLayoutManager(mLayoutManager);
                                recyclerView.setAdapter(mAdapter);
                                if (paymentHistoryBeans.size()>0){
                                    noDataAvailable.setVisibility(View.GONE);
                                }else {
                                    noDataAvailable.setText("No payment history available");
                                    noDataAvailable.setVisibility(View.VISIBLE);
                                }

                                //  AppUtil.showToast(getActivity(), response.getString("msg"), true);

                            } else {
                                noDataAvailable.setText("No payment history available");
                                noDataAvailable.setVisibility(View.VISIBLE);
                                Constant.showSnackBar(rootlayout, response.getString("msg"));
                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        mCustomProgressDialog.hideProgressDialog();
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                    }


                } catch (Exception e) {
                    mCustomProgressDialog.hideProgressDialog();
                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mCustomProgressDialog.hideProgressDialog();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));


            }
        });

    }

}


