package com.daf.c4chai.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daf.c4chai.MainActivity;
import com.daf.c4chai.R;
import com.daf.c4chai.bean.AllShopkeeperBean;
import com.daf.c4chai.fragments.adapter.ListFragmentAdapter;
import com.daf.c4chai.rest.ApiClient;
import com.daf.c4chai.rest.ApiInterface;
import com.daf.c4chai.utils.AppLog;
import com.daf.c4chai.utils.AppUtil;
import com.daf.c4chai.utils.Constant;
import com.daf.c4chai.utils.CustomProgressDialog;
import com.daf.c4chai.utils.SessionManager;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ListFragment extends Fragment implements ListFragmentAdapter.OnClickListener {

    Unbinder unbinder;

    @BindView(R.id.recyclerView_list)
    RecyclerView recyclerView;

    @BindView(R.id.rl_serch_list)
    RelativeLayout rl_serch_list;

    @BindView(R.id.serchview)
    EditText serchview;

    @BindView(R.id.iv_search_cancle)
    ImageView iv_search_cancle;

    @BindView(R.id.rootlayout)
    RelativeLayout rootlayout;

    @BindView(R.id.nodatafound)
    TextView nodatafound;

    private ArrayList<AllShopkeeperBean> allShopkeeperBeans = new ArrayList<>();
    SessionManager sessionManager;
    CustomProgressDialog mCustomProgressDialog;

    private ListFragmentAdapter mAdapter;
    LinearLayoutManager mLayoutManager;
    String activeInactiveStaus;
    String tea, coffee;

    public ListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        // bind view using butter knife
        unbinder = ButterKnife.bind(this, view);
        sessionManager = new SessionManager(getActivity());

        mCustomProgressDialog = new CustomProgressDialog(getActivity());
        mAdapter = new ListFragmentAdapter(allShopkeeperBeans, getActivity());
        mAdapter.setOnClickListener(this);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);

        if (AppUtil.isInternetAvailable(getActivity())) {
            getAllShopkeeper();
        } else {
            Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
        }


        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // unbind the view to free some memory
        unbinder.unbind();
    }

    @OnTextChanged(value = R.id.serchview, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void nameChanged(CharSequence text) {

        AppLog.v("changetext->", "hjgjhg" + text);
        mAdapter.getFilter().filter(text.toString());
        //do stuff
    }

    @OnClick({R.id.iv_search_cancle})
    public void onButtonClick(View view) {

        switch (view.getId()) {
            case R.id.iv_search_cancle:
                if (serchview.getText().toString().trim().equalsIgnoreCase("")) {
                    rl_serch_list.setVisibility(View.GONE);
                    ((MainActivity) getActivity()).ShowSerchbutton();
                } else {
                    serchview.setText("");
                }

                AppUtil.hideKeyboard(getActivity());
                break;
        }

    }

    @Override
    public void onPause() {

        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    public void changeButtonVisibility(boolean visibility) {
        if (visibility) {
            serchview.setText("");
            rl_serch_list.setVisibility(View.VISIBLE);
        } else {
            serchview.setText("");
            rl_serch_list.setVisibility(View.GONE);
        }
    }

    private void getAllShopkeeper() {
        mCustomProgressDialog.showProgressDialog("Loading");
        Log.e("Token", sessionManager.getUserToken());
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.getAllShopkeeper(sessionManager.getUserToken());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response1) {

                try {
                    if (response1.isSuccessful()) {
                        mCustomProgressDialog.hideProgressDialog();
                        allShopkeeperBeans.clear();
                        try {
                            String json = response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            if (response.getString("status").equalsIgnoreCase("200")) {
                                JSONArray dataArray = response.optJSONArray("data");
                                if (dataArray != null && dataArray.length()>0) {
                                    recyclerView.setVisibility(View.VISIBLE);
                                    nodatafound.setVisibility(View.GONE);
                                    for (int i = 0; i < dataArray.length(); i++) {
                                        JSONObject dataObject = dataArray.getJSONObject(i);
                                        allShopkeeperBeans.add(new AllShopkeeperBean(dataObject.optString("shop_id"),
                                                dataObject.optString("phone_number"),
                                                dataObject.optString("shopkeeper_name"),
                                                dataObject.optString("status"),
                                                dataObject.optString("total_tea_qty"),
                                                dataObject.optString("total_coffee_qty"),
                                                dataObject.optString("total_amount"),
                                                dataObject.optString("tea_rate"),
                                                dataObject.optString("coffee_rate"),
                                                dataObject.optString("shop_outstanding_balance"),
                                                dataObject.optString("user_id")));

                                    }
                                    mAdapter.notifyDataSetChanged();
                                }else{
                                    recyclerView.setVisibility(View.GONE);
                                    nodatafound.setVisibility(View.VISIBLE);
                                }
                                //  AppUtil.showToast(getActivity(), response.getString("msg"), true);

                            } else {
                                Constant.showSnackBar(rootlayout, response.getString("msg"));
                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        mCustomProgressDialog.hideProgressDialog();
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                    }


                } catch (Exception e) {
                    mCustomProgressDialog.hideProgressDialog();
                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mCustomProgressDialog.hideProgressDialog();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));


            }
        });

    }

    private void showNHideRecylerView() {

    }

    @Override
    public void onClick(String id, String shopkeeper_name, String teaRate, String coffeeRate, String status, int position) {
        updateShopkeeperDialog(id, shopkeeper_name, teaRate, coffeeRate, status, position);
    }

    private void updateShopkeeperDialog(final String id, String shopkeeper_name, String teaRate, String coffeeRate, final String status, final int position) {

        final Dialog d = new Dialog(getActivity());
        d.setContentView(R.layout.dialog_update_shopkeeper);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        d.setCancelable(true);
        d.getWindow().getAttributes().windowAnimations = R.style.Animations_SmileWindow;

        final TextView name = d.findViewById(R.id.txt_shopkeeper_name);
        final EditText teaEditCount = d.findViewById(R.id.edit_tea);
        final EditText coffeeEditCount = d.findViewById(R.id.edt_coffee);
        final RadioButton inActiveRadioButton = d.findViewById(R.id.inactiveButton);
        final RadioButton activeButton = d.findViewById(R.id.activeButton);
        TextView buttonAdd = d.findViewById(R.id.btnUpdate);

        if (status.equalsIgnoreCase("active")) {
            activeButton.setChecked(true);
            inActiveRadioButton.setChecked(false);
        }
        else
        {
            activeButton.setChecked(false);
            inActiveRadioButton.setChecked(true);
        }
        teaEditCount.setText(teaRate);
        coffeeEditCount.setText(coffeeRate);

        name.setText(shopkeeper_name);

        tea = teaRate;
        coffee = coffeeRate;
        activeButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                activeInactiveStaus = "active";
            }
        });

        inActiveRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                activeInactiveStaus = "inactive";
            }
        });

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (teaEditCount.getText().toString().trim().equalsIgnoreCase("") || teaEditCount.getText().toString().trim().startsWith("0"))

                {
                    tea = "0";
                }else {
                    tea = teaEditCount.getText().toString();
                }
                    if(coffeeEditCount.getText().toString().trim().equalsIgnoreCase("") || coffeeEditCount.getText().toString().trim().startsWith("0"))
                    {
                       coffee = "0";
                }else {
                    coffee = coffeeEditCount.getText().toString();
                    }
                    d.dismiss();
                        if (AppUtil.isInternetAvailable(getActivity())) {
                            if (activeButton.isChecked()) {
                                activeInactiveStaus = "active";
                            }else {
                                activeInactiveStaus = "inactive";
                            }

                            updateShopkeeperApi(id,tea, coffee, activeInactiveStaus,position );
                        } else {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
                        }


            }
        });

        d.show();
    }

    private void updateShopkeeperApi(final String id, final String teaRate, final String coffeeRate, final String status, final int position) {

        mCustomProgressDialog.showProgressDialog("Loading");
        String token = sessionManager.getUserToken();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.update_shopkeeper_profile(token, id, teaRate, coffeeRate, status);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response1) {

                try {
                    if (response1.isSuccessful()) {
                        mCustomProgressDialog.hideProgressDialog();
                        try {
                            String json = response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            if (response.getString("status").equalsIgnoreCase("200")) {
                                AppUtil.showToast(getActivity(), response.getString("msg"), true);
                               allShopkeeperBeans.get(position).setCoffee_rate(coffeeRate);
                               allShopkeeperBeans.get(position).setTea_rate(teaRate);
                               allShopkeeperBeans.get(position).setStatus(status);
                               mAdapter.notifyDataSetChanged();
                            } else {
                                Constant.showSnackBar(rootlayout, response.getString("msg"));
                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        mCustomProgressDialog.hideProgressDialog();
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                        AppLog.v("alltool", " not found");
                    }


                } catch (Exception e) {
                    mCustomProgressDialog.hideProgressDialog();
                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mCustomProgressDialog.hideProgressDialog();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));


            }
        });
    }
}