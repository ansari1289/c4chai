package com.daf.c4chai.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daf.c4chai.LoginActivity;
import com.daf.c4chai.R;
import com.daf.c4chai.VerifyOtpActivity;
import com.daf.c4chai.rest.ApiClient;
import com.daf.c4chai.rest.ApiInterface;
import com.daf.c4chai.utils.AppLog;
import com.daf.c4chai.utils.AppUtil;
import com.daf.c4chai.utils.Constant;
import com.daf.c4chai.utils.CustomProgressDialog;
import com.daf.c4chai.utils.SessionManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by faraz on 10/24/2018.
 */

public class AddShopKeeperFragment extends Fragment {
    Unbinder unbinder;

    @BindView(R.id.edt_shopkeepr_name)
    EditText edt_shopkeepr_name;

    @BindView(R.id.edt_phonenumber)
    EditText edt_phonenumber;

    @BindView(R.id.edt_tea_rate)
    EditText edt_tea_rate;

    @BindView(R.id.edt_coffee_rate)
    EditText edt_coffee_rate;

    @BindView(R.id.btn_add)
    TextView btn_add;

    @BindView(R.id.rootlayout)
    LinearLayout rootlayout;

    SessionManager sessionManager;
    CustomProgressDialog mCustomProgressDialog;

    public AddShopKeeperFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_shopkeeper, container, false);
        unbinder = ButterKnife.bind(this, view);
        sessionManager = new SessionManager(getActivity());
        mCustomProgressDialog = new CustomProgressDialog(getActivity());
        AppLog.v("mytoken","->"+sessionManager.getUserToken());

        return view;
    }

    @OnClick(R.id.btn_add)
    public void onButtonClick(View view) {
        Constant.hideKeyBoard(getActivity());
        if (edt_shopkeepr_name.getText().toString().trim().equalsIgnoreCase("")) {
            Constant.showSnackBar(rootlayout, getResources().getString(R.string.please_enter_shopkeepername));
        } else if (edt_phonenumber.getText().toString().trim().length() < 10) {
            Constant.showSnackBar(rootlayout, getResources().getString(R.string.please_enter_valid_number));
        } else {

            if (AppUtil.isInternetAvailable(getActivity())) {
                callAddShopkeeper(edt_shopkeepr_name.getText().toString().trim(), edt_phonenumber.getText().toString().trim(), edt_tea_rate.getText().toString().trim(), edt_coffee_rate.getText().toString().trim());
            } else {
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
            }


        }

    }

    private void callAddShopkeeper(String name, String number, String tearate, String coffeerate) {
        mCustomProgressDialog.showProgressDialog("Loading");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.addShopkeeper(sessionManager.getUserToken(), name, number, tearate, coffeerate);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response1) {

                try {
                    if (response1.isSuccessful()) {
                        mCustomProgressDialog.hideProgressDialog();
                        try {
                            String json = response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            if (response.getString("status").equalsIgnoreCase("200")) {
                                AppUtil.showToast(getActivity(), response.getString("msg"), true);
                                edt_coffee_rate.setText("");
                                edt_tea_rate.setText("");
                                edt_phonenumber.setText("");
                                edt_shopkeepr_name.setText("");
                            } else {
                                Constant.showSnackBar(rootlayout, response.getString("msg"));
                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        mCustomProgressDialog.hideProgressDialog();
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                    }


                } catch (Exception e) {
                    mCustomProgressDialog.hideProgressDialog();
                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mCustomProgressDialog.hideProgressDialog();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));
            }
        });

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

}
