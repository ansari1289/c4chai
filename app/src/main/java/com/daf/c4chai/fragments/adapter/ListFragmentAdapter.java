package com.daf.c4chai.fragments.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daf.c4chai.R;
import com.daf.c4chai.bean.AllShopkeeperBean;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ListFragmentAdapter extends RecyclerView.Adapter<ListFragmentAdapter.ViewHolder> implements Filterable {

    private final ArrayList<AllShopkeeperBean> mValues;
    private ArrayList<AllShopkeeperBean> mValuesFilter;
    Context context;
    private ItemFilter mFilter = new ItemFilter();

    OnClickListener onClickListener;

    public interface OnClickListener{
        void onClick(String id, String shopkeeper_name, String teaRate, String coffeeRate, String status, int position);
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public ListFragmentAdapter(ArrayList<AllShopkeeperBean> items, Context context) {
        mValues = items;
        mValuesFilter = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list_fragment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValuesFilter.get(position);
        holder.tv_name.setText(mValuesFilter.get(position).getShopkeeper_name());
        holder.tv_teacount.setText(""+(Integer.parseInt(mValuesFilter.get(position).getToday_tea_count())+ Integer.parseInt(mValuesFilter.get(position).getToday_coffee_count())));
        holder.tv_amount.setText(""+(Integer.parseInt(mValuesFilter.get(position).getTotal_amount())+ Integer.parseInt(mValuesFilter.get(position).getShop_outstanding_balance())));
        if (mValuesFilter.get(position).getStatus().equalsIgnoreCase("active"))
            holder.iv_active_inactive.setImageResource(R.drawable.active_shop);
        else
            holder.iv_active_inactive.setImageResource(R.drawable.inactive_shop);

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickListener!=null)
                    onClickListener.onClick(mValuesFilter.get(position).getShop_id(), mValuesFilter.get(position).getShopkeeper_name(), mValuesFilter.get(position).getTea_rate(), mValuesFilter.get(position).getCoffee_rate(), mValuesFilter.get(position).getStatus(), position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return mValuesFilter.size();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.tv_teacount)
        TextView tv_teacount;

        @BindView(R.id.tv_amount)
        TextView tv_amount;

        @BindView(R.id.tv_ratelist)
        ImageView tv_ratelist;


        @BindView(R.id.iv_active_inactive)
        ImageView iv_active_inactive;

        @BindView(R.id.rootView)
        RelativeLayout rootView;

        public AllShopkeeperBean mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);
        }

    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<AllShopkeeperBean> list = mValues;

            int count = list.size();
            final ArrayList<AllShopkeeperBean> nlist = new ArrayList<AllShopkeeperBean>(count);

            String filterableString ;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getShopkeeper_name();
                if (filterableString.toLowerCase().contains(filterString)) {
                    AllShopkeeperBean modelobj = list.get(i);
                    nlist.add(modelobj);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mValuesFilter = (ArrayList<AllShopkeeperBean>) results.values;
            notifyDataSetChanged();
        }

    }
}
