package com.daf.c4chai.fragments.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daf.c4chai.OrderPaymentActivity;
import com.daf.c4chai.PayActivity;
import com.daf.c4chai.R;
import com.daf.c4chai.bean.HomeFragmentBean;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HomeFragmentAdapter extends RecyclerView.Adapter<HomeFragmentAdapter.ViewHolder>  implements Filterable {

    private  ArrayList<HomeFragmentBean> mValues;
    private  ArrayList<HomeFragmentBean> mValuesFilter;
    Context context;
    private ItemFilter mFilter = new ItemFilter();
    OnAddListener onAddListener;

    public interface OnAddListener{
        void onAddClick(String id, String shopkeeper_name);
    }

    public void setOnAddListener(OnAddListener onAddListener) {
        this.onAddListener = onAddListener;
    }

    public HomeFragmentAdapter(ArrayList<HomeFragmentBean> items, Context context) {
        mValues = items;
        mValuesFilter=items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_home_fragment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValuesFilter.get(position);
        holder.tv_name.setText(mValuesFilter.get(position).getShopkeeper_name());
        holder.tv_teacount.setText(""+(Integer.parseInt(mValuesFilter.get(position).getToday_tea_count())+ Integer.parseInt(mValuesFilter.get(position).getToday_coffee_count())));

        holder.iv_teaadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onAddListener!=null)
                    onAddListener.onAddClick(mValuesFilter.get(position).getShop_id(), mValuesFilter.get(position).getShopkeeper_name());

            }
        });

        holder.iv_homepay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,PayActivity.class);
                intent.putExtra("shopkeeperId",mValuesFilter.get(position).getShop_id());
                intent.putExtra("shopkeeperName",mValuesFilter.get(position).getShopkeeper_name());
                context.startActivity(intent);
            }
        });
        holder.ll_center.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,OrderPaymentActivity.class);
                intent.putExtra("shopkeeperId",mValuesFilter.get(position).getShop_id());
                intent.putExtra("shopkeeperName",mValuesFilter.get(position).getShopkeeper_name());
                intent.putExtra("shopkeeperNumber",mValuesFilter.get(position).getPhone_number());
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return mValuesFilter.size();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<HomeFragmentBean> list = mValues;

            int count = list.size();
            final ArrayList<HomeFragmentBean> nlist = new ArrayList<HomeFragmentBean>(count);

            String filterableString ;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getShopkeeper_name();
                if (filterableString.toLowerCase().contains(filterString)) {
                    HomeFragmentBean modelobj = list.get(i);
                    nlist.add(modelobj);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mValuesFilter = (ArrayList<HomeFragmentBean>) results.values;
            notifyDataSetChanged();
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        @BindView(R.id.tv_name)
        TextView tv_name;
        @BindView(R.id.tv_teacount)
        TextView tv_teacount;
        @BindView(R.id.iv_teaadd)
        ImageView iv_teaadd;
        @BindView(R.id.iv_homepay)
        ImageView iv_homepay;

        @BindView(R.id.ll_center)
        LinearLayout ll_center;

        @BindView(R.id.root)
        LinearLayout ll_root;


        public HomeFragmentBean mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this, view);
        }

    }
}
