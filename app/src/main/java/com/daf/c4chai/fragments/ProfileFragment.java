package com.daf.c4chai.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daf.c4chai.R;
import com.daf.c4chai.rest.ApiClient;
import com.daf.c4chai.rest.ApiInterface;
import com.daf.c4chai.utils.AppLog;
import com.daf.c4chai.utils.AppUtil;
import com.daf.c4chai.utils.Constant;
import com.daf.c4chai.utils.CustomProgressDialog;
import com.daf.c4chai.utils.SessionManager;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProfileFragment extends Fragment {
    @BindView(R.id.editShoppkerName)
    EditText edName;

    @BindView(R.id.editPhoneNumber)
    TextView phoneNumber;

    @BindView(R.id.editCoffeeRate)
    EditText coffeeRate;

    @BindView(R.id.editTeaRate)
    EditText teaRate;

    @BindView(R.id.edit_iv)
    ImageView ivEdit;

    @BindView(R.id.cancel_iv)
    ImageView ivCancel;

    @BindView(R.id.submit_btn)
    TextView btnSubmit;

    @BindView(R.id.rootlayout)
    RelativeLayout rootlayout;

    Unbinder unbinder;
   /* @BindView(R.id.coffee_amount)
    TextView coffeeAmount;

    @BindView(R.id.previous_amount)
    TextView previousAmount;

    @BindView(R.id.current_amount)
    TextView currentAmount;

    @BindView(R.id.total_amount)
    TextView totalAmount;

    @BindView(R.id.editAmonut)
    EditText editAmount;

    @BindView(R.id.rootlayout)
    RelativeLayout rootlayout;

    @BindView(R.id.main_layout)
    RelativeLayout mainLayout;
*/

    SessionManager sessionManager;
    CustomProgressDialog mCustomProgressDialog;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view =  inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, view);

        sessionManager = new SessionManager(getActivity());
        mCustomProgressDialog = new CustomProgressDialog(getActivity());
       edName.setText(sessionManager.getUserName());
       phoneNumber.setText(sessionManager.getMobileNumber());
       teaRate.setText(sessionManager.getTeaRate());
       coffeeRate.setText(sessionManager.getCoffeeRate());

        edName.setEnabled(false);
        teaRate.setEnabled(false);
        coffeeRate.setEnabled(false);
        return view;
    }


    @OnClick(R.id.edit_iv)
    public void onEditClick(View view) {
        edName.setEnabled(true);
        teaRate.setEnabled(true);
        coffeeRate.setEnabled(true);
        ivEdit.setVisibility(View.GONE);
        ivCancel.setVisibility(View.VISIBLE);
        btnSubmit.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.cancel_iv)
    public void onCancelClick(View view) {
        edName.setEnabled(false);
        teaRate.setEnabled(false);
        coffeeRate.setEnabled(false);
        ivEdit.setVisibility(View.VISIBLE);
        ivCancel.setVisibility(View.GONE);
        btnSubmit.setVisibility(View.GONE);
    }

    @OnClick(R.id.submit_btn)
    public void onSubmitClick(View view) {
        if (edName.getText().toString().trim().equalsIgnoreCase("")) {
            Constant.showSnackBar(rootlayout, getResources().getString(R.string.please_enter_shopkeepername));
        }
        else if (teaRate.getText().toString().trim().equalsIgnoreCase("") || teaRate.getText().toString().trim().startsWith("0")) {
            Constant.showSnackBar(rootlayout, getResources().getString(R.string.please_enter_tea_amount));
        }  else if (coffeeRate.getText().toString().trim().equalsIgnoreCase("") || coffeeRate.getText().toString().trim().startsWith("0")) {
            Constant.showSnackBar(rootlayout, getResources().getString(R.string.please_enter_coffee_amount));
        }  else {
            edName.setEnabled(false);
            teaRate.setEnabled(false);
            coffeeRate.setEnabled(false);
            ivEdit.setVisibility(View.VISIBLE);
            ivCancel.setVisibility(View.GONE);
            btnSubmit.setVisibility(View.GONE);
            if (AppUtil.isInternetAvailable(getActivity())) {
                updateProfiletApi(edName.getText().toString().trim(),teaRate.getText().toString().trim(),coffeeRate.getText().toString().trim());
            } else {
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // unbind the view to free some memory
        unbinder.unbind();
    }

    private void updateProfiletApi(final String name, final String teaRate, final String coffeeRate) {

        mCustomProgressDialog.showProgressDialog("Loading");
        String token = sessionManager.getUserToken();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.updateProfile(token, name, teaRate, coffeeRate);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response1) {

                try {
                    if (response1.isSuccessful()) {
                        mCustomProgressDialog.hideProgressDialog();
                        try {
                            String json = response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            if (response.getString("status").equalsIgnoreCase("200")) {
                                AppUtil.showToast(getActivity(), response.getString("msg"), true);
                                sessionManager.setUserName(name);
                                sessionManager.setTeaRate(teaRate);
                                sessionManager.setCoffeeRate(coffeeRate);
                            } else {
                                Constant.showSnackBar(rootlayout, response.getString("msg"));
                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        mCustomProgressDialog.hideProgressDialog();
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                        AppLog.v("alltool", " not found");
                    }


                } catch (Exception e) {
                    mCustomProgressDialog.hideProgressDialog();
                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mCustomProgressDialog.hideProgressDialog();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));


            }
        });
    }

}
