package com.daf.c4chai.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daf.c4chai.MainActivity;
import com.daf.c4chai.R;
import com.daf.c4chai.bean.HomeFragmentBean;
import com.daf.c4chai.fragments.adapter.HomeFragmentAdapter;
import com.daf.c4chai.rest.ApiClient;
import com.daf.c4chai.rest.ApiInterface;
import com.daf.c4chai.utils.AppLog;
import com.daf.c4chai.utils.AppUtil;
import com.daf.c4chai.utils.Constant;
import com.daf.c4chai.utils.CustomProgressDialog;
import com.daf.c4chai.utils.SessionManager;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements HomeFragmentAdapter.OnAddListener {

    Unbinder unbinder;

    @BindView(R.id.recyclerview_home)
    RecyclerView recyclerView;

    @BindView(R.id.rl_serch_list)
    RelativeLayout rl_serch_list;

    @BindView(R.id.serchview)
    EditText serchview;

    @BindView(R.id.iv_search_cancle)
    ImageView iv_search_cancle;

    @BindView(R.id.nodatafound)
    TextView nodatafound;

    @BindView(R.id.rootlayout)
    RelativeLayout rootlayout;

    private ArrayList<HomeFragmentBean> homeList = new ArrayList<>();
    private HomeFragmentAdapter mAdapter;
    LinearLayoutManager mLayoutManager;
    SessionManager sessionManager;
    CustomProgressDialog mCustomProgressDialog;
    private boolean is_GloabalRateset = false;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        // bind view using butter knife
        unbinder = ButterKnife.bind(this, view);
        homeList.clear();
        sessionManager = new SessionManager(getActivity());
        mAdapter = new HomeFragmentAdapter(homeList, getActivity());
        mAdapter.setOnAddListener(this);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);

        mCustomProgressDialog = new CustomProgressDialog(getActivity());

        if (AppUtil.isInternetAvailable(getActivity())) {
            globalRateApi();
        } else {
            Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
        }


        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // unbind the view to free some memory
        unbinder.unbind();
    }

    @OnClick({R.id.iv_search_cancle})
    public void onButtonClick(View view) {

        switch (view.getId()) {
            case R.id.iv_search_cancle:
                if (serchview.getText().toString().trim().equalsIgnoreCase("")) {
                    rl_serch_list.setVisibility(View.GONE);
                    ((MainActivity) getActivity()).ShowSerchbutton();
                } else {
                    serchview.setText("");
                }

                AppUtil.hideKeyboard(getActivity());
                break;
        }

    }

    @OnTextChanged(value = R.id.serchview, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void nameChanged(CharSequence text) {

        AppLog.v("changetext->", "hjgjhg" + text);
        mAdapter.getFilter().filter(text.toString());
        //do stuff
    }

    public void changeButtonVisibility(boolean visibility) {
        if (visibility) {
            serchview.setText("");
            rl_serch_list.setVisibility(View.VISIBLE);
        } else {
            serchview.setText("");
            rl_serch_list.setVisibility(View.GONE);
        }
    }


    private void getShopkeeper() {
        if (!is_GloabalRateset)
        mCustomProgressDialog.showProgressDialog("Loading");
       Log.e("Token", sessionManager.getUserToken());
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.getShopkeeper(sessionManager.getUserToken());
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response1) {

                try {
                    if (response1.isSuccessful()) {
                        mCustomProgressDialog.hideProgressDialog();
                        homeList.clear();
                        try {
                            String json = response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            if (response.getString("status").equalsIgnoreCase("200")) {
                                JSONArray dataArray = response.optJSONArray("data");
                                if (dataArray!=null && dataArray.length()>0) {
                                    recyclerView.setVisibility(View.VISIBLE);
                                    nodatafound.setVisibility(View.GONE);

                                    for (int i = 0; i < dataArray.length(); i++) {
                                        JSONObject dataObject = dataArray.getJSONObject(i);
                                        homeList.add(new HomeFragmentBean(dataObject.optString("phone_number"),
                                                dataObject.optString("shopkeeper_name"),
                                                dataObject.optString("status"),
                                                dataObject.optString("today_tea_count"),
                                                dataObject.optString("today_coffee_count"),
                                                dataObject.optString("tea_rate"),
                                                dataObject.optString("coffee_rate"),
                                                dataObject.optString("shop_outstanding_balance"),
                                                dataObject.optString("user_id"),
                                                dataObject.optString("shop_id")));

                                    }
                                    mAdapter.notifyDataSetChanged();
                                }else{
                                    recyclerView.setVisibility(View.GONE);
                                    nodatafound.setVisibility(View.VISIBLE);
                                }
                                //  AppUtil.showToast(getActivity(), response.getString("msg"), true);

                            } else {
                                Constant.showSnackBar(rootlayout, response.getString("msg"));
                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        mCustomProgressDialog.hideProgressDialog();
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                    }


                } catch (Exception e) {
                    mCustomProgressDialog.hideProgressDialog();
                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mCustomProgressDialog.hideProgressDialog();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));


            }
        });

    }

    @Override
    public void onAddClick(String id, String shopkeeper_name) {
        addOrderDialog(id, shopkeeper_name);
    }

    private void addOrderDialog(final String id, String shopkeeper_name) {

        final Dialog d = new Dialog(getActivity());
        d.setContentView(R.layout.dialog_add_order);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        d.setCancelable(true);
        d.getWindow().getAttributes().windowAnimations = R.style.Animations_SmileWindow;

        final TextView date = d.findViewById(R.id.txt_date);
        final TextView name = d.findViewById(R.id.txt_shopkeeper_name);
        final EditText teaEditCount = d.findViewById(R.id.edit_tea);
        final EditText coffeeEditCount = d.findViewById(R.id.edt_coffee);
        TextView buttonAdd = d.findViewById(R.id.btndone);

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c);
        date.setText(formattedDate);
        name.setText(shopkeeper_name);

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (teaEditCount.getText().toString().trim().equalsIgnoreCase("") || teaEditCount.getText().toString().trim().startsWith("0"))

                {
                    if(coffeeEditCount.getText().toString().trim().equalsIgnoreCase("") || coffeeEditCount.getText().toString().trim().startsWith("0"))
                    {
                        Toast.makeText(getActivity(), "Please enter tea or coffee count", Toast.LENGTH_SHORT).show();
                    }else {
                        d.dismiss();
                        if (AppUtil.isInternetAvailable(getActivity())) {
                            addTeaCoffeeCountApi(id,teaEditCount.getText().toString().trim(), coffeeEditCount.getText().toString());
                        } else {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
                        }

                    }

                }    else {

                    d.dismiss();
                    if (AppUtil.isInternetAvailable(getActivity())) {
                        addTeaCoffeeCountApi(id,teaEditCount.getText().toString().trim(), coffeeEditCount.getText().toString());
                    } else {
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
                    }

                }
            }
        });

        d.show();
    }
    private void addTeaCoffeeCountApi(String id, String teaAmount, String coffeeAmount) {

        mCustomProgressDialog.showProgressDialog("Loading");
        String token = sessionManager.getUserToken();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.addOrder(token, id, teaAmount, coffeeAmount);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response1) {

                try {
                    if (response1.isSuccessful()) {
                        mCustomProgressDialog.hideProgressDialog();
                        try {
                            String json = response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            if (response.getString("status").equalsIgnoreCase("200")) {
                                AppUtil.showToast(getActivity(), response.getString("msg"), true);
                                if (AppUtil.isInternetAvailable(getActivity())) {
                                    getShopkeeper();
                                } else {
                                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
                                }
                            } else {
                                Constant.showSnackBar(rootlayout, response.getString("msg"));
                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        mCustomProgressDialog.hideProgressDialog();
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                        AppLog.v("alltool", " not found");
                    }


                } catch (Exception e) {
                    mCustomProgressDialog.hideProgressDialog();
                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mCustomProgressDialog.hideProgressDialog();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));


            }
        });
    }


    private void addGlobalRateDialog() {

        final Dialog d = new Dialog(getActivity());
        d.setContentView(R.layout.dialog_add_coffee_tea_amount);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        d.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        d.setCancelable(false);
        d.getWindow().getAttributes().windowAnimations = R.style.Animations_SmileWindow;
        final EditText teaEditAmount = d.findViewById(R.id.editTeaAmount);
        final EditText coffeeEditAmount = d.findViewById(R.id.editCoffeeAmount);
        TextView buttonAdd = d.findViewById(R.id.btndone);

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (teaEditAmount.getText().toString().trim().equalsIgnoreCase("") || teaEditAmount.getText().toString().trim().startsWith("0")) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.please_enter_tea_amount), Toast.LENGTH_SHORT).show();
                }  else if (coffeeEditAmount.getText().toString().trim().equalsIgnoreCase("") || coffeeEditAmount.getText().toString().trim().startsWith("0")) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.please_enter_coffee_amount), Toast.LENGTH_SHORT).show();
                }  else {
                    d.dismiss();
                    if (AppUtil.isInternetAvailable(getActivity())) {
                        addTeaCoffeeRateApi(teaEditAmount.getText().toString().trim(), coffeeEditAmount.getText().toString().toString());
                    } else {
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
                    }

                }
            }
        });
        d.show();
    }

    private void addTeaCoffeeRateApi(final String teaAmount, final String coffeeAmount) {

        mCustomProgressDialog.showProgressDialog("Loading");
        String token = sessionManager.getUserToken();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.addItemsRate(token, teaAmount, coffeeAmount);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response1) {

                try {
                    if (response1.isSuccessful()) {
                        mCustomProgressDialog.hideProgressDialog();
                        try {
                            String json = response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            JSONObject myobj = response.getJSONObject("data");
                            if (response.getString("status").equalsIgnoreCase("200")) {
                                AppUtil.showToast(getActivity(), response.getString("msg"), true);
                                if (AppUtil.isInternetAvailable(getActivity())) {
                                    getShopkeeper();
                                } else {
                                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
                                }
                                sessionManager.setCoffeeRate(coffeeAmount);
                                sessionManager.setTeaRate(teaAmount);
                            } else {
                                Constant.showSnackBar(rootlayout, response.getString("msg"));
                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        mCustomProgressDialog.hideProgressDialog();
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                        AppLog.v("alltool", " not found");
                    }


                } catch (Exception e) {
                    mCustomProgressDialog.hideProgressDialog();
                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mCustomProgressDialog.hideProgressDialog();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));


            }
        });
    }


    private void globalRateApi() {
        mCustomProgressDialog.showProgressDialog("Loading");
        String token = sessionManager.getUserToken();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.checkGlobalRate(token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response1) {
                try {
                    if (response1.isSuccessful()) {
                        try {
                            String json = getResponseBody(response1);//response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            if (response.getString("status").equalsIgnoreCase("200")) {
                                //  AppUtil.showToast(MainActivity.this, response.getString("msg"), true);
                                JSONObject dataObject = response.getJSONObject("data");
                                String tea = dataObject.getString("Tea");
                                String coffee = dataObject.getString("Coffee");
                                if (tea.equalsIgnoreCase("-1")&& coffee.equalsIgnoreCase("-1"))
                                {
                                    mCustomProgressDialog.hideProgressDialog();

                                    is_GloabalRateset = false;
                                    addGlobalRateDialog();
                                }else {
                                    sessionManager.setCoffeeRate(coffee);
                                    sessionManager.setTeaRate(tea);
                                    if (AppUtil.isInternetAvailable(getActivity())) {
                                        is_GloabalRateset = true;
                                        getShopkeeper();
                                    } else {
                                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.PleaseCheckIntenetconection));
                                    }
                                }

                            } else {
                                mCustomProgressDialog.hideProgressDialog();

                                Constant.showSnackBar(rootlayout, response.getString("msg"));
                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        mCustomProgressDialog.hideProgressDialog();
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                        AppLog.v("alltool", " not found");
                    }


                } catch (Exception e) {
                    mCustomProgressDialog.hideProgressDialog();
                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mCustomProgressDialog.hideProgressDialog();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));


            }
        });
    }

    public static String getResponseBody(Response<ResponseBody> response) {

        BufferedReader reader = null;
        String output = null;
        try {

            if (response.body() != null) {
                reader = new BufferedReader(new InputStreamReader(response.body().byteStream()));
            } else if (response.errorBody() != null) {
                reader = new BufferedReader(new InputStreamReader(response.errorBody().byteStream()));
            }
            output = reader.readLine();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return output;
    }
}