package com.daf.c4chai.bean;

public class PaymentHistoryBean {
    private String user_id;
    private String shop_id;
    private String amount;
    private String payment_date;

    public PaymentHistoryBean(String user_id, String shop_id, String amount, String payment_date) {
        this.user_id = user_id;
        this.shop_id = shop_id;
        this.amount = amount;
        this.payment_date = payment_date;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPayment_date() {
        return payment_date;
    }

    public void setPayment_date(String payment_date) {
        this.payment_date = payment_date;
    }
}
