package com.daf.c4chai.bean;

/**
 * Created by faraz on 10/23/2018.
 */

public class HomeFragmentBean {
   private String phone_number;
   private String shopkeeper_name;
   private String status;
   private String today_tea_count;
   private String today_coffee_count;
   private String tea_rate;
   private String coffee_rate;
   private String shop_outstanding_balance;
   private String user_id;
   private String shop_id;

    public HomeFragmentBean(String phone_number, String shopkeeper_name, String status, String today_tea_count, String today_coffee_count, String tea_rate, String coffee_rate, String shop_outstanding_balance, String user_id, String shop_id) {
        this.phone_number = phone_number;
        this.shopkeeper_name = shopkeeper_name;
        this.status = status;
        this.today_tea_count = today_tea_count;
        this.today_coffee_count = today_coffee_count;
        this.tea_rate = tea_rate;
        this.coffee_rate = coffee_rate;
        this.shop_outstanding_balance = shop_outstanding_balance;
        this.user_id = user_id;
        this.shop_id = shop_id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getShopkeeper_name() {
        return shopkeeper_name;
    }

    public void setShopkeeper_name(String shopkeeper_name) {
        this.shopkeeper_name = shopkeeper_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToday_tea_count() {
        return today_tea_count;
    }

    public void setToday_tea_count(String today_tea_count) {
        this.today_tea_count = today_tea_count;
    }

    public String getToday_coffee_count() {
        return today_coffee_count;
    }

    public void setToday_coffee_count(String today_coffee_count) {
        this.today_coffee_count = today_coffee_count;
    }

    public String getTea_rate() {
        return tea_rate;
    }

    public void setTea_rate(String tea_rate) {
        this.tea_rate = tea_rate;
    }

    public String getCoffee_rate() {
        return coffee_rate;
    }

    public void setCoffee_rate(String coffee_rate) {
        this.coffee_rate = coffee_rate;
    }

    public String getShop_outstanding_balance() {
        return shop_outstanding_balance;
    }

    public void setShop_outstanding_balance(String shop_outstanding_balance) {
        this.shop_outstanding_balance = shop_outstanding_balance;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
