package com.daf.c4chai.bean;

/**
 * Created by faraz on 10/21/2018.
 */

public class UserInformationBean {
    String user_id;
    String mobile;
    String token;
    String name;
    String rate_tea;
    String rate_coffee;

    public UserInformationBean(String name, String user_id, String mobile, String token,String rate_tea,String rate_coffee) {
        this.name = name;
        this.user_id = user_id;
        this.mobile = mobile;
        this.token = token;
        this.rate_coffee=rate_coffee;
        this.rate_tea=rate_tea;
    }

    public String getRate_tea() {
        return rate_tea;
    }

    public void setRate_tea(String rate_tea) {
        this.rate_tea = rate_tea;
    }

    public String getRate_coffee() {
        return rate_coffee;
    }

    public void setRate_coffee(String rate_coffee) {
        this.rate_coffee = rate_coffee;
    }

    public String getUsername() {
        return name;
    }

    public void setUsername(String username) {
        this.name = username;
    }

    public String getId() {
        return user_id;
    }

    public void setId(String id) {
        this.user_id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
