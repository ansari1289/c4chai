package com.daf.c4chai.bean;

public class OrderBean {
      private String user_id;
      private String shop_id;
      private String order_date;
      private String total_amount;
      private String order_quantity;

    public OrderBean(String user_id, String shop_id, String order_date, String total_amount, String order_quantity) {
        this.user_id = user_id;
        this.shop_id = shop_id;
        this.order_date = order_date;
        this.total_amount = total_amount;
        this.order_quantity = order_quantity;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getOrder_quantity() {
        return order_quantity;
    }

    public void setOrder_quantity(String order_quantity) {
        this.order_quantity = order_quantity;
    }
}
