package com.daf.c4chai.bean;

public class OrderDetailBean {
    private String timeslot;
    private String tea_qty;
    private String coffee_qty;

    public OrderDetailBean(String timeslot, String tea_qty, String coffee_qty) {
        this.timeslot = timeslot;
        this.tea_qty = tea_qty;
        this.coffee_qty = coffee_qty;
    }


    public String getTimeslot() {
        return timeslot;
    }

    public void setTimeslot(String timeslot) {
        this.timeslot = timeslot;
    }

    public String getTea_qty() {
        return tea_qty;
    }

    public void setTea_qty(String tea_qty) {
        this.tea_qty = tea_qty;
    }

    public String getCoffee_qty() {
        return coffee_qty;
    }

    public void setCoffee_qty(String coffee_qty) {
        this.coffee_qty = coffee_qty;
    }
}
