package com.daf.c4chai.rest;

import com.google.gson.JsonObject;


import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("user/sendOtp")
    Call<JsonObject> sendOtp(@Header("token") String Token, @Field("mobile") String mobilenumber,@Field("device_id") String deviceID, @Field("firebase_token") String firebasetoken);

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("user/checkOtp")
    Call<JsonObject> verifyotp(@Header("token") String Token, @Field("mobile") String mobilenumber,@Field("otp") String otp);

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("user/add_items_rate")
    Call<JsonObject> addItemsRate(@Header("access_token") String Token, @Field("tea_rate") String teaRate,@Field("coffee_rate") String coffeeRate);

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("user/add_shopkeeper")
    Call<JsonObject> addShopkeeper(@Header("access_token") String Token, @Field("name") String name,@Field("phone_number") String phone_number,@Field("tea_rate") String tea_rate,@Field("coffee_rate") String coffee_rate);

    @Headers({"Accept: application/json"})
    @POST("user/shopkeeper")
    Call<JsonObject> getShopkeeper(@Header("access_token") String Token);

    @Headers({"Accept: application/json"})
    @POST("user/check_global_rate")
    Call<ResponseBody> checkGlobalRate(@Header("access_token") String Token);

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("user/add_order")
    Call<JsonObject> addOrder(@Header("access_token") String Token, @Field("shopkeeperid") String shopkeeperid,@Field("tea_count") String tea_rate,@Field("coffee_count") String coffee_count);

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("user/order_list")
    Call<JsonObject> orderList(@Header("access_token") String Token, @Field("shop_id") String shopkeeperid);

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("user/orderlist_date")
    Call<JsonObject> orderDateList(@Header("access_token") String Token, @Field("shop_id") String shopkeeperid, @Field("date") String date);

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("Order/get_payment_history")
    Call<JsonObject> getPaymentHistory(@Header("access_token") String Token, @Field("shop_id") String shopkeeperid);

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("Order/get_payment_details")
    Call<JsonObject> getPaymentDetails(@Header("access_token") String Token, @Field("shop_id") String shopkeeperid);

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("Order/add_payment")
    Call<JsonObject> addPayment(@Header("access_token") String Token, @Field("shop_id") String shopkeeperid, @Field("amount") String amount);

    @Headers({"Accept: application/json"})
    @POST("user/all_shopkeeper")
    Call<JsonObject> getAllShopkeeper(@Header("access_token") String Token);

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("user/update_profile")
    Call<JsonObject> updateProfile(@Header("access_token") String Token, @Field("name") String name, @Field("tea_rate") String teaRate, @Field("coffee_rate") String coffeeRate);

    @Headers({"Accept: application/json"})
    @FormUrlEncoded
    @POST("user/update_shopkeeper_profile")
    Call<JsonObject> update_shopkeeper_profile(@Header("access_token") String Token, @Field("shop_id") String shop_id, @Field("tea_rate") String teaRate, @Field("coffee_rate") String coffeeRate, @Field("status") String status);
    /*    @FormUrlEncoded
    @POST("user/add_shopkeeper")
    Call<JsonObject> add_shopkeeper(@Header("access_token") String Token, @Field("name") String name,@Field("phone_number") String phone_number,@Field("tea_rate") String tea_rate,@Field("coffee_rate") String coffee_rate);*/
}
