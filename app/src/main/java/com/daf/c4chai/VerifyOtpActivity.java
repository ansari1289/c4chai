package com.daf.c4chai;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.daf.c4chai.ConectionCheck.SmsListenerReciver;
import com.daf.c4chai.bean.UserInformationBean;
import com.daf.c4chai.interfaces.ReadSmsInterface;
import com.daf.c4chai.rest.ApiClient;
import com.daf.c4chai.rest.ApiInterface;
import com.daf.c4chai.utils.AppLog;
import com.daf.c4chai.utils.AppUtil;
import com.daf.c4chai.utils.Constant;
import com.daf.c4chai.utils.SessionManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by faraz on 10/21/2018.
 */

public class VerifyOtpActivity extends AppCompatActivity {

    @BindView(R.id.edt_otp)
    EditText edt_otp;

    @BindView(R.id.rootlayout)
    RelativeLayout rootlayout;
    SessionManager sessionManager;
    String mobilenumber;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifyotp);
        ButterKnife.bind(this);
        sessionManager=new SessionManager(VerifyOtpActivity.this);
        mobilenumber=getIntent().getStringExtra("mobilenumber");
        requestSmsPermission();

        SmsListenerReciver.bindListener(new ReadSmsInterface.OTPListener() {
            @Override
            public void onOTPReceived(String extractedOTP) {
                edt_otp.setText(extractedOTP);

                if(AppUtil.isInternetAvailable(VerifyOtpActivity.this))
                callVerifyOtpApi(edt_otp.getText().toString().trim());
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        SmsListenerReciver.unbindListener();
    }

    private void requestSmsPermission() {

        Dexter.withActivity(this)
                .withPermission(android.Manifest.permission.RECEIVE_SMS)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted
                       // MoveLoginActivity();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            showSettingsDialog();
                        }else {

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(VerifyOtpActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @OnClick({R.id.btn_verify, R.id.tv_resend_otp})
    public void onButtonClick(View view) {

        switch(view.getId()){
            case R.id.btn_verify:

                Constant.hideKeyBoard(VerifyOtpActivity.this);
                if (edt_otp.getText().toString().trim().equalsIgnoreCase("")) {
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.please_enter_otp));
                } else if (edt_otp.getText().toString().trim().length() < 6) {
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.please_enter_valid_otp));
                } else {
                    callVerifyOtpApi(edt_otp.getText().toString().trim());
                   // startActivity(new Intent(VerifyOtpActivity.this, VerifyOtpActivity.class));
                   // finish();
                }
                break;
            case R.id.tv_resend_otp:
                //callApi();
                break;
        }

    }

    private void callVerifyOtpApi(String otp) {
        final ProgressDialog progressDialog = new ProgressDialog(VerifyOtpActivity.this);
        progressDialog.setTitle(getResources().getString(R.string.pleasewait));
        progressDialog.setCancelable(false);
        progressDialog.show();

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.verifyotp(Constant.TOKEN_WITHOUTLOGIN, mobilenumber,otp);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response1) {
                progressDialog.dismiss();
                try {
                    if (response1.isSuccessful()) {
                        try {
                            String json = response1.body().toString();
                            JSONObject response = null;
                            response = new JSONObject(json);
                            Log.v("responseforgotpass", "" + response);
                            if (response.getString("status").equalsIgnoreCase("200")) {

                                JSONObject jsonObject = response.getJSONObject("data");
                                Gson gson = new GsonBuilder().serializeNulls().create();
                                //UserInformationBean userInformationBean = new UserInformationBean();
                                UserInformationBean userInformationBean = gson.fromJson(String.valueOf(jsonObject), UserInformationBean.class);
                                AppLog.v("userInformationBean", userInformationBean.getMobile());
                                sessionManager.createSession(userInformationBean.getId(), userInformationBean.getToken(), userInformationBean.getUsername(), userInformationBean.getMobile(),userInformationBean.getRate_tea(),userInformationBean.getRate_coffee());
                                Intent intent = new Intent(VerifyOtpActivity.this, MainActivity.class);
                                startActivity(intent);
                                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                                finish();

                              //  AppUtil.showToast(VerifyOtpActivity.this, response.getString("msg"), true);

                            } else {
                                Constant.showSnackBar(rootlayout, response.getString("msg"));

                            }

                        } catch (Exception e) {
                            Constant.showSnackBar(rootlayout, getResources().getString(R.string.RuntimeException));
                            e.printStackTrace();
                        }
                    } else {
                        Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));
                        AppLog.v("alltool", " not found");
                    }


                } catch (Exception e) {

                    e.printStackTrace();
                    Constant.showSnackBar(rootlayout, getResources().getString(R.string.ServerError));

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
                Constant.showSnackBar(rootlayout, getResources().getString(R.string.ApiFailureError));


            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(VerifyOtpActivity.this, LoginActivity.class));
        finish();
    }
}


