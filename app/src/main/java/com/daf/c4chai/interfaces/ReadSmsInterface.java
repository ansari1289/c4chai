package com.daf.c4chai.interfaces;

/**
 * Created by faraz on 10/27/2018.
 */

public interface ReadSmsInterface {
    interface OTPListener {
        void onOTPReceived(String otp);
    }
}
