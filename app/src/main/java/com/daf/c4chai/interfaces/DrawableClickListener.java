package com.daf.c4chai.interfaces;

/**
 * Created by faraz on 10/29/2018.
 */

public interface DrawableClickListener {
    public static enum DrawablePosition { TOP, BOTTOM, LEFT, RIGHT };
    public void onClick(DrawablePosition target);
}
